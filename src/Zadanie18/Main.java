package Zadanie18;

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person("Ada", "Arasz", 25);
        Person person2 = new Person("Filip", "Wasylew", 15);
        Club club = new Club();
        club.enter(person1);
        club.enter(person2);

    }
}
