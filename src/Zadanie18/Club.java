package Zadanie18;

public class Club {

    public void enter(Person person) {
        if (person.getAge() >= 18) {
            System.out.println("Możesz wejść");
        }else {
            throw new NoAdultException();
        }
    }
}
