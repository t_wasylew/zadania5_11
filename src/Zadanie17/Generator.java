package Zadanie17;

public class Generator {
    private int number;
    private int minValue;
    private int maxValue;
    private char letter;
    private char tmpLetter;
    private int tempNumber;
    private boolean tmpBoolean;


    public Generator(char letter) {
        this.letter = letter;
        tmpLetter = letter;
    }

    public Generator(int minValue, int maxValue) {
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    public Generator(int number) {
        this.number = number;
        tempNumber = number;
        tmpBoolean = true;
    }

    public char getTmpLetter() {
        return tmpLetter;
    }

    public void setTmpLetter(char tmpLetter) {
        this.tmpLetter = tmpLetter;
    }

    public boolean isTmpBoolean() {
        return tmpBoolean;
    }

    public void setTmpBoolean(boolean tmpBoolean) {
        this.tmpBoolean = tmpBoolean;
    }

    public int getTempNumber() {
        return tempNumber;
    }

    public void setTempNumber(int tempNumber) {
        this.tempNumber = tempNumber;
    }

    public int getMinValue() {
        return minValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void generateNumber() {
        setNumber(getNumber() + 1);
        System.out.println(getNumber());
    }

    public void generateNumbers() {
        for (int i = getMinValue(); i < getMaxValue() - 1; i++) {
            System.out.println(i + 1);
        }
    }

    public void generateEvenNumbers() {

        for (int i = getMinValue(); i < getMaxValue() - 1; i++) {
            if ((i + 1) % 2 == 0) {
                System.out.println(i + 1);
            }
        }
    }

    public void generateMultiplyNumbers(int number) {
        int numbers = getTempNumber();
        numbers += number;
        setTempNumber(numbers);
        System.out.println(numbers);
    }

    public void generateTrueFalse() {
        setTmpBoolean(!isTmpBoolean());
        System.out.println(isTmpBoolean());
    }


    public void generateNextLetters() {
        if (getTmpLetter() < 'z') {

            System.out.println((char) (getTmpLetter() + 1));
            setTmpLetter((char) (getTmpLetter() + 1));
        } else {
            System.out.println("Jestes na końcu alfabetu");
        }
    }
}
