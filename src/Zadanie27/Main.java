package Zadanie27;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Map<Integer, Boolean> booleanMap = new HashMap<>();

        for (int i = 1; i <= 30; i++) {
            booleanMap.put(i, isPrime(i));
        }

        for (Map.Entry<Integer, Boolean> entry : booleanMap.entrySet()) {
            System.out.println("Liczba: "+ entry.getKey() + " " + entry.getValue()+".");
        }
    }

    private static boolean isPrime(int i) {
        for (int j = 2; j < i/2; j++) {
            if (i % j != 0) {
                return true;
            }
        }
        return false;
    }
}
