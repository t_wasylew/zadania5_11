package Zadanie19;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<FamilyMember> familyMembers = new ArrayList<>();
        familyMembers.add(new Father());
        familyMembers.add(new Mother());
        familyMembers.add(new Son());
        familyMembers.add(new Daughter());

        for (int i = 0; i < familyMembers.size(); i++) {
            familyMembers.get(i).introduce();
        }
    }
}
