package Zadanie19;

public abstract class FamilyMember {

    private String name;

    public void introduce() {
        System.out.println("I am just a simple family member");
    }

    public abstract boolean isAdult();
}
