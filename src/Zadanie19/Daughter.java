package Zadanie19;

public class Daughter extends FamilyMember {

    @Override
    public void introduce() {
        System.out.println("I am daughter");
    }

    @Override
    public boolean isAdult() {
        return false;
    }

}
