package Zadanie19;

public class Father extends FamilyMember {

    @Override
    public void introduce() {
        System.out.println("I am father");
    }

    @Override
    public boolean isAdult() {
        return true;
    }
}
