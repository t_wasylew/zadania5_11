package Zadanie19;

public class Mother extends FamilyMember {

    @Override
    public void introduce() {
        System.out.println("I am mother");
    }

    @Override
    public boolean isAdult() {
        return true;
    }
}
