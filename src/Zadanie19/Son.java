package Zadanie19;

public class Son extends FamilyMember {

    @Override
    public void introduce() {
        System.out.println("I am son");
    }

    @Override
    public boolean isAdult() {
        return false;
    }
}
